import React from "react";
import { Link, Outlet } from "react-router-dom";

const Root = () => {
  return (
    <>
      <header className="shadow-lg h-14 px-8 flex flex-row justify-between items-center bg-blue-100">
        <div className="flex flex-row items-center text-xl text-cyan-800 font-bold">
          <img
            className="h-10 "
            src="/images/TuneLogo.png"
            alt="spotify-logo"
          />
          <span>Tune Tide</span>
        </div>
        <nav>
          <ul className="flex flex-row gap-4 items-center font-semibold text-sm text-black">
            <Link>Premium</Link>
            <Link>Support</Link>
            <Link>Download</Link>
            <span className="border-l-2 border-black">&nbsp;</span>
            <Link>Sign Up</Link>
            <Link>Log In</Link>
          </ul>
        </nav>
      </header>
      <main className="min-h-screen">
        <Outlet />
      </main>
      <footer className=" bg-slate-100 px-12">
        <div className="flex justify-between items-center px-16">
          <div className="flex flex-col items-center py-4">
          <img className=" h-32" src="/images/TuneLogo.png" alt="" />
          <span className="text-xl text-cyan-800 font-bold">Tune Tide</span>
          </div>
          <ul className="flex gap-4 font-semibold ">
            <Link>Home</Link>
            <Link>About Us</Link>
            <Link>Contact Us</Link>
            <Link>Help</Link>
          </ul>
          <div>
            <ul className="flex gap-4 items-center">
              <Link ><img className="w-8" src="/images/fb.png" alt="" /></Link>
              <Link className="hover:invert"><img className="w-8" src="/images/insta.png" alt="" /></Link>
              <Link><img className="w-8" src="/images/X.png" alt="" /></Link>
            </ul>
          </div>
          
        </div>
        <div className=" px-20">
        <hr className="border-slate-400 "/>
        </div>
        <div className="text-center py-6 font-semibold"><span> &copy; Copyright. All Rights Reserved</span></div>
      </footer>
    </>
  );
};

export default Root;
