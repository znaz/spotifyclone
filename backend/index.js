const express = require("express");
const app = express();
require("dotenv").config();
require("express-async-errors");
const port = process.env.PORT || 3000;
const connectDb = require("./Connection/database");
const cors = require("cors");
const cookieParser = require("cookie-parser");

app.use(cors());
app.use(cookieParser());
app.use(express.json());

const Users = require("./Routes/user");

app.use("/users", Users);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
